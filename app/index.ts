import express from 'express';
import { MongoClient } from 'mongodb';

const url = 'mongodb://localhost:27017'
const client = new MongoClient(url)

const dbName = 'myProject';
const collectionName = 'test';
const db = client.db(dbName);

const app = express();
const PORT = 3000;

app.get('/', (req, res) => {
  res.send('Hello world');
});

app.get('/add_safe/:num', async (req, res) => {
    let val = parseInt(req.params['num']);
    let doc:any = await db.collection(collectionName).findOneAndUpdate(
        {name: 'val1'},
        {$inc: {
            value: val
        }},
        {
            returnDocument: 'after'
        }
    )
    res.send("val: " + doc.value["value"])
})

app.get('/add_unsafe/:num', async (req, res) => {
    let val = parseInt(req.params['num']);
    let oldval:any = await db.collection(collectionName).findOne({name: 'val2'})
    let doc:any = await db.collection(collectionName).findOneAndUpdate(
        {name: 'val2'},
        {$set: {
            value: oldval["value"] + val
        }},
        {
            returnDocument: 'after'
        }
    )
    res.send("val: " + doc.value["value"])
    
})

app.get('/get_safe', async (req, res) => {
    let oldval:any = await db.collection(collectionName).findOne({name: 'val1'})
    res.send("val: " + oldval["value"])
})

app.get('/get_unsafe', async (req, res) => {
    let oldval:any = await db.collection(collectionName).findOne({name: 'val2'})
    res.send("val: " + oldval["value"])
})

//setup db

async function run(){
    await client.connect();
    await db.collection(collectionName).deleteMany({});
    await db.collection(collectionName).insertOne({
        name: 'val1',
        value: 0
    });
    await db.collection(collectionName).insertOne({
        name: 'val2',
        value: 0
    });
    app.listen(PORT, () => {
        console.log(`Express with Typescript! http://localhost:${PORT}`);
    });
}

run()
